<?php


function pb_submission_form_validate($form, &$form_state){

	if ($form_state['triggering_element']['#value'] == 'Back') {
		return;
	}

	switch ($form_state['stage']) {

		case 'choose_category':
			return pb_submission_choose_category_validate($form, $form_state);
		break;

		case 'create_fieldnote':
			return pb_submission_create_fieldnote_validate($form, $form_state);
		break;

		case 'personal_info':
			return pb_submission_personal_info_validate($form, $form_state);
		break;

	}

}

function pb_submission_choose_category_validate ($form, &$form_state) {

	if(!$form_state['values']['category']) {
		form_set_error('category', 'You must select a category before continuing.');
	}

	// if "Other" is selected, make input mandatory
	if(($form_state['values']['category'] == 36) && (!$form_state['values']['other'])) {
		form_set_error('other', 'Please fill in a custom tag.');
	}

}

function pb_submission_create_fieldnote_validate ($form, &$form_state) {

	if(!$form_state['values']['title']) {
		form_set_error('title', 'Please fill in a title.');
	}

	if((!$form_state['values']['image']) && (!$form_state['values']['medialink'])) {

		// Require at least some type of content to be submitted
		if (!$form_state['values']['story']['value']) {
			form_set_error('story', 'Please fill in at least one of the following: image, multimedia link, or body.');
			form_set_error('image', '');
			form_set_error('medialink', '');
		}

		// Require summary only if no media present
		if (!$form_state['values']['summary']) {
			form_set_error('summary', 'Please fill in a summary, or add an image or media link.');
		}

	}

}

function pb_submission_personal_info_validate ($form, &$form_state) {

	if(!$form_state['values']['name']) {
		form_set_error('name', 'Please fill in a name.');
	}

	if(!$form_state['values']['email']) {
		form_set_error('email', 'Please fill in an email.');
	}
	else {
		if (!valid_email_address($form_state['values']['email'])) {
			form_set_error('email', 'Please enter a valid email address.');
		}
	}

}