<?php


function pb_submission_get_header($form, &$form_state) {

	$form_state['stage'] = isset($form_state['stage']) ? $form_state['stage'] : 1;

	$form_stages = array(
		'choose_category' => 1,
		'create_fieldnote' => 2,
		'personal_info' => 3,
	);

	if (isset($form_stages[$form_state['stage']])) {
		$current_step = $form_stages[$form_state['stage']]; 
	}
	else {
		$current_step = 1;
	}
	
	$stages = array(
		1 => array('data' => '1. Select Category'),
		2 => array('data' => '2. Create Field Note'),
		3 => array('data' => '3. Personal Info'),
	);

	$stages[$current_step]['class'] = array('active');

	$stages_list = theme('item_list', array('items' => $stages, 'type' => 'ol'));

	$form['header'] = array(
		'#type' => 'fieldset',
		'#title' => '',
		'#description' => $stages_list
	);

	return $form;

}

function pb_submission_next_stage($form, &$form_state) {

	switch ($form_state['stage']) {

		case 'choose_category':
			return 'create_fieldnote';
		break;

		case 'create_fieldnote':
			return 'personal_info';
		break;

	}

}

function pb_submission_previous_stage($form, &$form_state) {

	switch ($form_state['stage']) {

		case 'create_fieldnote':
			return 'choose_category';
		break;

		case 'personal_info':
			return 'create_fieldnote';
		break;

	}

}