<?php


function pb_submission_form($form, &$form_state){
	drupal_add_js(drupal_get_path('module','pb_submission') . '/pb_submission.js');
	drupal_add_css(drupal_get_path('module','pb_submission') . '/pb_submission.css');

	if (!isset($form_state['stage'])) $form_state['stage'] = 'choose_category';

	$form = array(
		'#prefix' => '<div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 content-main"><h1 class="page-header">Submit a Field Note</h1>',
		'#suffix' => '</div>',
	);

	$form = pb_submission_get_header($form, $form_state);

	switch ($form_state['stage']) {

		case 'choose_category':
			return pb_submission_choose_category_form($form, $form_state);
		break;

		case 'create_fieldnote':
			return pb_submission_create_fieldnote_form($form, $form_state);
		break;

		case 'personal_info':
			return pb_submission_personal_info_form($form, $form_state);
		break;

		default:
			return pb_submission_choose_category_form($form, $form_state);
		break;

	}

	return $form;
}

function pb_submission_choose_category_form($form, &$form_state) {

	$values = isset($form_state['multistep_values']['choose_category']) ? $form_state['multistep_values']['choose_category'] : array();

	$form['category'] = array(
		'#title' => 'Select Category',
		'#type' => 'radios',
		'#options' => array ( // numbers correspond to TIDs
			25 => t('Food Note'),
			35 => t('River Note'),
			33 => t('Sound Note'),
			34 => t('Street Note'),
			26 => t('Tree Note'),
			36 => t('Other')
		),
		'#default_value' => isset($values['category']) ? $values['category'] : NULL,
		'#weight' => 1
	);

	$form['other'] = array(
		'#type' => 'textfield',
		'#attributes' => array(
			'placeholder' => t('If choosing Other, please create a custom tag for your Field Note.')
		),
		'#default_value' => isset($values['other']) ? $values['other'] : NULL,
		'#weight' => 2
	);

	$form['next'] = array(
		'#type' => 'submit',
		'#value' => t('Next'),
		'#weight' => 3
	);

	return $form;

}

function pb_submission_create_fieldnote_form($form, &$form_state) {

	$selected_category = $form_state['multistep_values']['choose_category']['category'];
	if (!isset($selected_category)) $selected_category = 0;

	switch($selected_category) {

		case '25':
			$selected_tag = 'Food Note';
			$selected_prompt = '<p>Food, in particular, is central to place—the smells and tastes, the traditions and histories. Food gathers people together to commune. What does that look and feel like in Philadelphia?</p>
				<p>Here are some questions to get you thinking. Feel free to respond to one, several, or to take your own approach.</p>
				<p>What are the recipes of your Philadelphia?<br>
				When you think of Philly, do particular memories of smells associated with food come to mind?<br>
				What are the stories and histories around the food you cook and eat?<br>
				What are the restaurants you loved but no longer exist?<br>
				What kinds of food are available to you in your neighborhood and what are not?</p>';
		break;

		case '35':
			$selected_tag = 'River Note';
			$selected_prompt = '<p>Philadelphia is a city of rivers running through it.</p>
				<p>In <em>Still Water (The River Thames for Example)</em>, artist Roni Horn photographed The River Thames over a period of time, capturing its various conditions and differences. Then, she overlaid the images with footnotes, adding bits of text that speak to the histories and dreams, facts and fictions embedded within the river.</p>
				<p>What footnote would you add to the Delaware or Schuylkill Rivers?<br>
				Is it a memory, a dream, or a piece of little known history?<br>
				What happens alongside the rivers?<br>
				How does water impact your relationship to Philadelphia?</p>';
		break;

		case '33':
			$selected_tag = 'Sound Note';
			$selected_prompt = '<p>What is the sound of your Philadelphia?</p>
				<p>Here are some questions to get you thinking. Feel free to respond to one, several, or to take your own approach.</p>
				<p>Is there a song or musician that encapsulates the feeling of this city for you?<br>
				If you were going to make a Philly playlist, what would it include?<br>
				Share your list of songs and/or direct links to YouTube or SoundCloud below.</p>
				<p>Or is it the sounds you hear everyday outside your window? What are they?<br>
				What does Philadelphia sound like to you? What narratives and histories do these sounds speak to?</p>';
		break;

		case '34':
			$selected_tag = 'Street Note';
			$selected_prompt = '<p>Tell us the stories, myths, and memories of the streets where you live, walk, and grew up.</p>
				<p>Here are some questions to get you thinking. Feel free to respond to one, several, or to take your own approach.</p>
				<p>What do you see, hear, feel on Philadelphia’s streets?<br>
				How have the streets of Philly changed over time?<br>
				What encounters have you experienced on the streets?<br>
				What are the untold stories of your street?</p>';
		break;

		case '26':
			$selected_tag = 'Tree Note';
			$selected_prompt = '<p>Re-PLACE-ing Artist/Thinker Marty Pottenger has encouraged us to think through the ecology of Philadelphia through her research. Marty is asking, “What if people got to re-anchor, re-place, in their personal relationships and narratives with nature?”</p>
				<p>Is there a tree in Philadelphia that you feel close to? What is the story of that tree? </p>
				<p>Or, if your Philadelphia is not one of trees and green spaces, what impact does that have on your relationship to the city?</p>';
		break;

		case '36':
			$selected_tag = 'Custom Note';
			$selected_prompt = '<p>Think About:<br>Philadelphia, its neighborhoods, places and spaces.</p>
				<p>Share:<br>
				What is the Philadelphia you remember?<br>
				What do you love about Philadelphia today?<br>
				What is the Philadelphia you imagine?</p>';
		break;

	}

	$values = isset($form_state['multistep_values']['create_fieldnote']) ? $form_state['multistep_values']['create_fieldnote'] : array();

	$form['fieldnote'] = array(
		'#type' => 'fieldset',
		'#title' => 'Create Your ' . $selected_tag,
		'#description' => $selected_prompt,
	);

	$form['fieldnote']['title'] = array(
		'#type' => 'textfield',
		'#title' => 'Title *',
		'#default_value' => isset($values['title']) ? $values['title'] : NULL,
	);

	$form['fieldnote']['summary'] = array(
		'#type' => 'textfield',
		'#title' => 'Summary',
		'#description' => 'Enter a brief summary or keywords to describe your field note - this is only required if you do not have an image or multimedia link uploaded. Maximum of 140 characters.',
		'#default_value' => isset($values['summary']) ? $values['summary'] : NULL,
	);

	$form['content'] = array(
		'#type' => 'fieldset',
		'#description' => t('<p>Fill out one or more of the following fields to create your field note.</p>'),
		'#title' => t('Add Media'),
		'#weight' => 2
	);

	$form['content']['image'] = array(
		'#title' => t('Image'),
		'#type' => 'managed_file',
		'#description' => t('Drag an image from your computer or browse to the file and upload it to feature an image in your field note. Files must be less than 8MB. Allowed file types: png gif jpg jpeg.'),
		'#default_value' => isset($values['image']) ? $values['image'] : NULL,
		'#upload_location' => 'public://uploaded/',
	);

	$form['content']['medialink'] = array(
		'#type' => 'textfield',
		'#title' => 'Multimedia Link',
		'#description' => t('Enter the link to your video or audio field note hosted on YouTube, Vimeo, or SoundCloud. A thumbnail will be automatically generated for you, but you may override this by uploading an image above.'),
		'#default_value' => isset($values['medialink']) ? $values['medialink'] : NULL,
		'#attributes' => array(
			'placeholder' => 'http://'
		)
	);

	$form['content']['story'] = array(
		'#type' => 'text_format',
		'#title' => 'Body',
		'#description' => '<p class="help-block">Enter the body of your text field note or, if desired, a longer description of your image, video, or audio field note. Basic formatting options are available.</p>',
		'#default_value' => isset($values['story']) ? $values['story'] : NULL,
		'#format' => 'filtered_html'
	);

	$form['location'] = array(
		'#type' => 'fieldset',
		'#title' => 'Location (optional, click to expand)',
		'#description' => '<p>If your Field Note has a specific location that you would like to tag, insert its address here.</p>',
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
		'#weight' => 3
	);

	$form['location']['address1'] = array(
		'#type' => 'textfield',
		'#title' => 'Address 1',
		'#default_value' => isset($values['address1']) ? $values['address1'] : NULL,
	);

	$form['location']['address2'] = array(
		'#type' => 'textfield',
		'#title' => 'Address 2',
		'#default_value' => isset($values['address2']) ? $values['address2'] : NULL,
	);

	$form['location']['city'] = array(
		'#type' => 'textfield',
		'#title' => 'City',
		'#default_value' => isset($values['city']) ? $values['city'] : 'Philadelphia',
	);

	$form['location']['state'] = array(
		'#type' => 'select',
		'#title' => 'State',
		'#default_value' => isset($values['state']) ? $values['state'] : 'PA',
		'#options' => array(
			'AL' => 'Alabama', 'AK' => 'Alaska', 'AZ' => 'Arizona', 'AR' => 'Arkansas', 'CA' => 'California', 'CO' => 'Colorado', 'CT' => 'Connecticut', 'DE' => 'Delaware', 'DC' => 'District Of Columbia', 'FL' => 'Florida', 'GA' => 'Georgia', 'HI' => 'Hawaii', 'ID' => 'Idaho', 'IL' => 'Illinois', 'IN' => 'Indiana', 'IA' => 'Iowa', 'KS' => 'Kansas', 'KY' => 'Kentucky', 'LA' => 'Louisiana', 'ME' => 'Maine', 'MD' => 'Maryland', 'MA' => 'Massachusetts', 'MI' => 'Michigan', 'MN' => 'Minnesota', 'MS' => 'Mississippi', 'MO' => 'Missouri', 'MT' => 'Montana', 'NE' => 'Nebraska', 'NV' => 'Nevada', 'NH' => 'New Hampshire', 'NJ' => 'New Jersey', 'NM' => 'New Mexico', 'NY' => 'New York', 'NC' => 'North Carolina', 'ND' => 'North Dakota', 'OH' => 'Ohio', 'OK' => 'Oklahoma', 'OR' => 'Oregon', 'PA' => 'Pennsylvania', 'RI' => 'Rhode Island', 'SC' => 'South Carolina', 'SD' => 'South Dakota', 'TN' => 'Tennessee', 'TX' => 'Texas', 'UT' => 'Utah', 'VT' => 'Vermont', 'VA' => 'Virginia', 'WA' => 'Washington', 'WV' => 'West Virginia', 'WI' => 'Wisconsin', 'WY' => 'Wyoming'),
	);

	$form['location']['zip'] = array(
		'#type' => 'textfield',
		'#title' => 'Zip',
		'#default_value' => isset($values['zip']) ? $values['zip'] : NULL,
	);

	$form['back'] = array(
		'#type' => 'submit',
		'#value' => t('Back'),
		'#weight' => 10
	);

	$form['next'] = array(
		'#type' => 'submit',
		'#value' => t('Next'),
		'#weight' => 11
	);

	return $form;

}

function pb_submission_personal_info_form($form, &$form_state) {

	$values = isset($form_state['multistep_values']['personal_info']) ? $form_state['multistep_values']['personal_info'] : array();

	$form['personal'] = array(
		'#type' => 'fieldset',
		'#title' => 'Personal',
		'#description' => t('Your personal information. This information will not be published.'),
	);

	$form['personal']['name'] = array(
		'#type' => 'textfield',
		'#title' => 'Name',
		'#weight' => 1,
		'#default_value' => isset($values['name']) ? $values['name'] : NULL,
	);

	$form['personal']['email'] = array(
		'#type' => 'textfield',
		'#title' => 'Email',
		'#weight' => 2,
		'#default_value' => isset($values['email']) ? $values['email'] : NULL,
	);

	$form['back'] = array(
		'#type' => 'submit',
		'#value' => t('Back')
	);

	$form['next'] = array(
		'#type' => 'submit',
		'#value' => t('Submit'),
	);

	return $form;

}