<?php

// Handles 'back' and 'next' buttons
function pb_submission_form_submit($form, &$form_state) {

	switch($form_state['stage']) {

		case 'personal_info':
			$form_state['multistep_values'][$form_state['stage']] = $form_state['values'];
			if ($form_state['triggering_element']['#value'] != 'Back') {
				pb_submission_personal_info_form_submit($form, $form_state);
				$form_state['complete'] = TRUE;
			}
		break;

		default:
			$form_state['multistep_values'][$form_state['stage']] = $form_state['values'];
			$form_state['new_stage'] = pb_submission_next_stage($form, $form_state);
		break;

	}

	if (isset($form_state['complete'])) drupal_goto('complete-page');
	
	if ($form_state['triggering_element']['#value'] == 'Back') {
		$form_state['new_stage'] = pb_submission_previous_stage($form, $form_state);
	}
	
	if (isset($form_state['multistep_values']['form_build_id'])) {
		$form_state['values']['form_build_id'] = $form_state['multistep_values']['form_build_id'];
	}

	$form_state['multistep_values']['form_build_id'] = $form_state['values']['form_build_id'];
	$form_state['stage'] = $form_state['new_stage'];
	$form_state['rebuild'] = TRUE;

}

// Handles final submission
function pb_submission_personal_info_form_submit($form, &$form_state) {

	$multstep_values = $form_state['multistep_values'];

	$category = $multstep_values['choose_category']['category'];
	$other = $multstep_values['choose_category']['other'];

	$title = $multstep_values['create_fieldnote']['title'];
	$summary = $multstep_values['create_fieldnote']['summary'];
	$image = $multstep_values['create_fieldnote']['image'];
	$medialink = $multstep_values['create_fieldnote']['medialink'];
	$story = $multstep_values['create_fieldnote']['story']['value'];
	$address1 = $multstep_values['create_fieldnote']['address1'];
	$address2 = $multstep_values['create_fieldnote']['address2'];
	$city = $multstep_values['create_fieldnote']['city'];
	$state = $multstep_values['create_fieldnote']['state'];
	$zip = $multstep_values['create_fieldnote']['zip'];

	$email = $multstep_values['personal_info']['email'];
	$name = $multstep_values['personal_info']['name'];


// Create new node
	$node = new StdClass();
	$node->type = 'field_note';
	$node->status = 0;

	// title
	$node->title = $title;
	$node->field_note_summary['und'][0]['value'] = $summary;

	// category
	$node->field_note_tags['und'][0]['tid'] = $category;
	$node->field_note_tags_other['und'][0]['value'] = $other;


	// field note content
	$node->body['und'][0] = array(
		'value' => $story,
		'format' => 'filtered_html'
	);

	if($medialink) {
		$file = pb_submission_file_handler($medialink); // adds file to database from url
		$node->field_note_media['und'][0] = (array)$file; // saves to media field
	}

	if($image) {
		$node->field_note_image['und'][0] = array(
			'fid' => $image,
			'display' => 1,
			'description' => '',
			'status' => 1
		);
	}

	// locations
	$node->field_location['und'][0]['thoroughfare'] = $address1;
	$node->field_location['und'][0]['premise'] = $address2;
	$node->field_location['und'][0]['locality'] = $city;
	$node->field_location['und'][0]['administrative_area'] = $state;
	$node->field_location['und'][0]['postal_code'] = $zip;

	// author info
	$node->field_email['und'][0]['value'] = $email;
	$node->field_display_name['und'][0]['value'] = $name;

	node_submit($node);
	node_save($node);

	if($node->nid) {
		drupal_set_message('<p>Thank you for your field note submission and for contributing your story to Re-PLACE-ing Philadelphia.</p><p>You will receive an email when your field note is live on the website.</p>', 'positive-feedback');
	}
	else {
		drupal_set_message('<p>There was an error creating your field note. If this problem persists, please contact an administrator.</p>', 'positive-feedback');
	}

}

function pb_submission_file_handler($url) {

	if (strpos($url,'youtube') !== false) {
    	$source = 'youtube';
	}
	if (strpos($url,'vimeo') !== false) {
		$source = 'vimeo';
	}
	if (strpos($url,'soundcloud') !==false) {
		$source = 'soundcloud';
	}

	switch($source) {

		case 'youtube':
		    module_load_include('inc', 'media_youtube', 'includes/MediaInternetYouTubeHandler.inc');
    		$obj = new MediaInternetYouTubeHandler($url);
		break;

		case 'vimeo':
			module_load_include('inc', 'media_vimeo', 'includes/MediaInternetVimeoHandler.inc');
			$obj = new MediaInternetVimeoHandler($url);
		break;

		case 'soundcloud':
			module_load_include('inc', 'media_soundcloud', 'includes/MediaInternetSoundCloudHandler.inc');
			$obj = new MediaInternetSoundCloudHandler($url);
		break;

	}

    $file = $obj->getFileObject();
    $file->display = 1;
    file_save($file);
    return $file;

}