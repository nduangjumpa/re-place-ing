(function($) {

$(document).ready(function(){
	$('#edit-other').hide();

	$('#edit-category-36').change(function() {
		if(($(this)).is(':checked')) {
			$('#edit-other').show();
		}
		else {
			$('#edit-other').hide();
		}
	});

});

})(jQuery);