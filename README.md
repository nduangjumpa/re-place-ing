Re-PLACE-ing Philadelphia Field Note
=====
This module was developed for [Re-PLACE-ing Philadelphia](http://re-place-ing.org) at [I-SITE, Inc.](http://i-site.com).

About the Module
-----
Re-PLACE-ing Philadelphia is a Painted Bride program funded by the PEW Center. The event was set to run from October 2015 until April 2016, during which time the center accepted submissions from the public about their Philadelphia-centric stories. These stories, displayed as Field Notes, could be categorized under different types of notes (Food, River, Sound, Street, Tree, Other) and could feature different types of media (image, video, audio, prose). This module serves as a custom, interactive form to capture and display each visitor's Field Note.

[View Live Demo](http://re-place-ing.org/submit)

Features
-----
* Multistep form which results in an individual Field Note, which is dynamically displayed based on uploaded content.
* Predefined categories with customized writing prompts and the ability for users to create their own.
* Integration with YouTube, Vimeo, and SoundCloud for multimedia content.

Dependencies
-----
* Drupal 7.x
* [Media module](https://www.drupal.org/project/media)